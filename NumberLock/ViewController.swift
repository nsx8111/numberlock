//
//  ViewController.swift
//  NumberLock
//
//  Created by ITC on 2017/12/1.
//  Copyright © 2017年 ITC. All rights reserved.
//

import UIKit
import GameKit

class ViewController: UIViewController {
    
    //0~99 + 1 = 1~100   make a random number 取亂數1~100
    var answer = GKRandomSource.sharedRandom().nextInt(upperBound:101)
    var maxNumber = 100
    var minNumber = 0
    var gameOver = false
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    @IBOutlet weak var messageLabel2: UILabel!
    
    @IBOutlet weak var messageLabel: UILabel!
    
    @IBOutlet weak var overLabel: UILabel!
    
    @IBOutlet weak var inputTextField: UITextField!
    
    @IBOutlet weak var background: UIImageView!
    
    @IBAction func makeAGuess(_ sender: UIButton) {
        
        if gameOver == false{
            print(answer)
            
            messageLabel2.text = ""
            
            //take input text out
            let inputText = inputTextField.text!
            //        print("inputText = \(inputText)")
            
            //清除數字
            inputTextField.text = ""
            
            let inputNumber = Int(inputText)
            
            if inputNumber == nil{
                //輸入錯誤
                messageLabel.text = "請輸入數字 \(minNumber) 到 \(maxNumber)"
            }else{
                //輸入正確
                if inputNumber! > maxNumber{
                    //數字超過最大範圍
                    messageLabel.text = "你智障喔！ \(minNumber) 到 \(maxNumber)"
                }else if inputNumber! < minNumber{
                    //數字低於最小範圍
                    messageLabel.text = "你智障喔！ \(minNumber) 到 \(maxNumber)"
                }else if inputNumber! == answer{
                    //Bingo! 正確答案
                    messageLabel2.text = "啊哈哈哈哈哈哈哈～～"
                    messageLabel.text = ""
                    overLabel.text = "中"
                    gameOver = true
                    background.image = UIImage(named: "249556_153e13695601480a200e715e5b749")
                }else {
                    //沒中
                    if inputNumber! == 94{
                        messageLabel2.text = "9 4 8 7    9 4 5 3"
                    }
                    if inputNumber! == 87{
                        messageLabel2.text = "北七是吧！"
                    }
                    if inputNumber! == 78{
                        messageLabel2.text = "你現在是在罵誰！？"
                    }
                    if inputNumber! == 77{
                        messageLabel2.text = "老子是道家，墨子是墨家，七七是乳加"
                    }
                    if inputNumber! == 69{
                        messageLabel2.text = "矮油～你在想什麼啊～"
                    }
                    if inputNumber! == 45{
                        messageLabel2.text = "是你嗎～是我嗎～"
                    }
                    if inputNumber! == 33{
                        messageLabel2.text = "姍姍來遲，這就是姍姍的不對了！"
                    }
                    if inputNumber! == 22{
                        messageLabel2.text = "餓餓～人家肚子餓餓啦！"
                    }
                    if inputNumber! == 21{
                        messageLabel2.text = "二一兩次就GG惹～"
                    }
                    if inputNumber! == 17{
                        messageLabel2.text = "8 + 9 就是17"
                    }
                    if inputNumber! == 11{
                        messageLabel2.text = "Always Open , 7-ELEVEN"
                    }
                    if inputNumber! == 7{
                        messageLabel2.text = "Always Open , 7-ELEVEN"
                    }
                    
                    if inputNumber! > answer{
                        maxNumber = inputNumber!
                    }else{
                        minNumber = inputNumber!
                    }
                    
                    if maxNumber - minNumber <= 10 {
                        if maxNumber - minNumber <= 2{
                            messageLabel.text = "耶耶耶耶～虛驚一場！   \(minNumber) 到 \(maxNumber)"
                        }else{
                            messageLabel.text = "太可怕了！ \(minNumber) 到 \(maxNumber)"
                        }
//                        messageLabel2.text = "耶耶耶耶耶耶～～"
                    }else{
                        messageLabel.text = "\(minNumber) 到 \(maxNumber)"
                    }
                    
                }
            }
        }else{
            //遊戲結束
            messageLabel2.text = ""
            overLabel.text = ""
            maxNumber = 100
            minNumber = 0
            messageLabel.text = "請輸入數字 \(minNumber) 到 \(maxNumber)"
            answer = GKRandomSource.sharedRandom().nextInt(upperBound:101)
            gameOver = false
            background.image = UIImage(named: "YSnoV")
        }
        
    }
    
    @IBAction func clear(_ sender: UIButton) {
        //重新遊戲
        messageLabel2.text = ""
        overLabel.text = ""
        maxNumber = 100
        minNumber = 0
        messageLabel.text = "請輸入數字 \(minNumber) 到 \(maxNumber)"
        answer = GKRandomSource.sharedRandom().nextInt(upperBound:101)
        gameOver = false
        background.image = UIImage(named: "YSnoV")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //push the keyboard up
        inputTextField.becomeFirstResponder()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

